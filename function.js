//Fahmi Amin Nudin
//181351078
//Pagi B

//01
var teriak = () => { return ("Halo Humanika!"); }
console.log(teriak());
console.log("");

//02
//let num1 = 12;
//let num2 = 4;
//let kalikan = (num1, num2) => { return num1 * num2 }
//console.log(kalikan(num1, num2));
//console.log("");

//03
//var name = "Agus";
//var age = 30;
//var address = "Jln. Malioboro, Yogyakarta"
//var hobby = "Gaming"
//var introduce = (name, age, address, hobby) => { return (`Nama saya ` + name + `, umur saya ` + age + ` tahun, ` + `alamat saya di ` + address + `, dan saya punya hobby yaitu ` + hobby + `!`) }
//var perkenalan = introduce(name, age, address, hobby);
//console.log(perkenalan);
//console.log("");