import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const ExTouch = () => {
  const [count, setCount] = useState(0);
  const onPress = () => setCount((prevCount) => prevCount + 1);

  return (
    <View
      style={{
        flexDirection: 'row',
        backgroundColor: '#1EF3B3',
        height: 812,
        width: 375,
      }}>
      <View
        style={{
          backgroundColor: '#1E27F3',
          position: 'absolute',
          width: 140,
          height: 31,
          left: 48,
          top: 406,
        }}>
        <Text
          style={{
            fontSize: 24,
            fontFamily: 'Rochester',
            fontWeight: 'normal',
            color: '#1E27F3',
            width: 319,
            lineHeight: 31,
            textAlign: '',
            backgroundColor: '#1EF3B3',
          }}>
          Username/Email
        </Text>
      </View>
      <View
        style={{
          backgroundColor: '#E5E5E5',
          borderRadius: 8,
          width: 287,
          height: 36,
          left: 48,
          top: 437,
          position: 'absolute',
        }}>
        <Text
          style={{
            fontSize: 24,
            fontFamily: 'Rochester',
            fontWeight: 'normal',
            color: '#1E27F3',
            width: 300,
            lineHeight: 31,
            textAlign: 'center',
            color: '#000000',
          }}>
          dhafin.rr@gmail.com
        </Text>
      </View>

      <View
        style={{
          backgroundColor: '#1E27F3',
          position: 'absolute',
          width: 80,
          height: 32,
          left: 48,
          top: 488,
        }}>
        <Text
          style={{
            fontSize: 24,
            fontFamily: 'Rochester',
            fontWeight: 'normal',
            color: '#1E27F3',
            width: 319,
            lineHeight: 32,
            textAlign: '',
            backgroundColor: '#1EF3B3',
          }}>
          Password
        </Text>
      </View>
      <View
        style={{
          backgroundColor: '#E5E5E5',
          borderRadius: 8,
          width: 287,
          height: 36,
          left: 46,
          top: 520,
          position: 'absolute',
        }}>
        <Text
          style={{
            fontSize: 24,
            fontFamily: 'Rochester',
            fontWeight: 'normal',
            color: '#1E27F3',
            width: 300,
            lineHeight: 31,
            textAlign: 'center',
            color: '#000000',
          }}>
          ********
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
});

export default ExTouch;
