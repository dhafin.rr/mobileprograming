import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';

const onPressTitle = () => {
  console.log('title pressed');
};

const ExText = () => {
  return (
    <View style={{ backgroundColor: '#1EF3B3', height: 812, width: 375 }}>
      <View style={{ backgroundColor: '#1EF3B3' }} />
      <Text
        style={{
          fontSize: 36,
          fontFamily: 'Redressed',
          fontWeight: 'normal',
          color: '#E5E5E5',
          width: 319,
          height: 84,
          left: 32,
          top: 24,
          textAlign: 'center',
          backgroundColor: '#1EF3B3',
        }}>
        Let's to go climb Mountain Indonesia
      </Text>
    </View>
  );
};

export default ExText;
