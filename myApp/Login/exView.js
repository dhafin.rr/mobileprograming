import React from 'react';
import { View, Text } from 'react-native';

const ExView = () => {
  return (
    <View
      style={{
        flexDirection: 'row',
        height: 812,
        width: 375,
      }}>
      <View style={{ backgroundColor: '#1EF3B3', flex: 99 }} />
    </View>
  );
};

export default ExView;
