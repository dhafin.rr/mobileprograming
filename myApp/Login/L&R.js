import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const ExTouch = () => {
  const [count, setCount] = useState(0);
  const onPress = () => setCount((prevCount) => prevCount + 1);

  return (
    <View
      style={{
        flexDirection: 'row',
        backgroundColor: '#1EF3B3',
        height: 812,
        width: 375,
      }}>
      <View
        style={{
          backgroundColor: '#E5E5E5',
          borderRadius: 8,
          width: 155,
          height: 31,
          left: 111,
          top: 587,
          position: 'absolute',
        }}>
        <Text
          style={{
            fontSize: 18,

            fontFamily: 'Rochester',
            fontWeight: 'normal',
            color: '#1E27F3',
            width: 160,
            lineHeight: 23,
            textAlign: 'center',
            color: '#11C82F',
          }}>
          LOGIN
        </Text>
      </View>

      <View
        style={{
          backgroundColor: '#E5E5E5',
          borderRadius: 8,
          width: 155,
          height: 31,
          left: 111,
          top: 649,
          position: 'absolute',
        }}>
        <Text
          style={{
            fontSize: 18,

            fontFamily: 'Rochester',
            fontWeight: 'normal',
            color: '#1E27F3',
            width: 160,
            lineHeight: 23,
            textAlign: 'center',
            color: '#11C82F',
          }}>
          REGISTRASI
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
});

export default ExTouch;
