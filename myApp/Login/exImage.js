import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingTop: 100,
    height: 812,
    width: 375,
    backgroundColor: '#1EF3B3',
  },
  logo: {
    width: 375,
    height: 241,
    top: 115,
    left: 2,
  },
});

const ExImage = () => {
  return (
    <View style={styles.container}>
      <Image
        source={require('./ff.png')}
        style={{ width: 300, height: 150, top: 50, left: 25, right: 25 }}
        resizeMode="contain"
      />
    </View>
  );
};

export default ExImage;
